<?php

namespace stop;

function enqueue_assets()
{
    wp_enqueue_style('foundation', get_template_directory_uri() . '/dist/foundation.min.css');
    wp_enqueue_style('app', get_template_directory_uri() . '/dist/app.css', [], filemtime(get_template_directory() . '/dist/app.css'));

    wp_enqueue_script('main-app', get_template_directory_uri() . '/dist/app.js', ['jquery'], filemtime(get_template_directory() . '/dist/app.js'), true);
    wp_enqueue_script('cookie', get_template_directory_uri() . '/assets/javascript/js.cookie.js', ['jquery'], false, true);
    wp_enqueue_script('acessibilidade', get_template_directory_uri() . '/assets/javascript/acessibilidade.js', ['jquery', 'cookie'], false, true);
    wp_enqueue_script('youtube-plataform', 'https://apis.google.com/js/platform.js');
    wp_register_style( 'slick-css', get_template_directory_uri() . '/assets/vendor/slick/css/slick.min.css', '', '1.6.1', '' );
    wp_register_style( 'slick-theme-css', get_template_directory_uri() . '/assets/vendor/slick/css/slick-theme.min.css', '', '1.6.1', '' );
    wp_enqueue_style( 'slick-css' );
    wp_enqueue_style( 'slick-theme-css' );
    wp_register_script( 'slick-js', get_template_directory_uri() . '/assets/vendor/slick/js/slick.min.js', array( 'jquery' ), '1.6.1', true );
    wp_enqueue_script( 'slick-js' );
}

add_action('wp_enqueue_scripts', 'stop\\enqueue_assets');
