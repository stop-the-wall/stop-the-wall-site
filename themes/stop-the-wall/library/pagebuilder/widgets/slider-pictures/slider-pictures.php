<?php
/*
  Widget Name: Pictures slider
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class PicturesSlider extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __( 'Title', 'stop')
            ),

            'repetidor_fotos' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'stop'),
                'fields' => array(
                    'legend' => array(
                        'type' => 'text',
                        'label' => __( 'Legend', 'stop')
                    ),

                    'author' => array(
                        'type' => 'text',
                        'label' => __( 'Author', 'stop')
                    ),

                    'author_text' => array(
                        'type' => 'text',
                        'label' => __( 'Author of the text', 'stop')
                    ),

                    'location' => array(
                        'type' => 'text',
                        'label' => __( 'Location', 'stop')
                    ),
                    
                    'picture' => array(
                        'type' => 'media',
                        'label' => __( 'Picture', 'stop' ),
                        'choose' => __( 'Pick an image', 'stop'),
                        'update' => __( 'Set an image', 'stop'),
                        'library' => 'image',
                        'fallback' => true
                    ),

                    'huge_text' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Image text', 'stop' ),
                        'rows' => 10,
                        'default_editor' => 'html',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    )

                )
            )
        ];

        parent::__construct('slider-pictures', 'Pictures slider', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('slider-pictures', __FILE__, 'widgets\PicturesSlider');
