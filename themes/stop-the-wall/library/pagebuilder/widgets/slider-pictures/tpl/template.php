<section class="pictures-slider">
    <div class="row">
        <div class="column large-12">
            <div class="section-title">
                <?= $instance['title'] ?>
            </div>
        </div>
    </div>
    <div class="pictures">
        <button class="close-button">
            <i class="fas fa-times"></i>
        </button>
        <div class="itens">
            <?php foreach ($instance['repetidor_fotos'] as $index=>$block) : ?>
                <div class="slider">
                    <div class="image-bg">
                        <button type="button" data-role="none" class="single-item-collection--attachments-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>
                        <button type="button" data-role="none" class="single-item-collection--attachments-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>
                        <div class="row">
                            <div class="column large-10 margin-auto">
                                <div class="image-cut">
                                    <button class="expand-icon">
                                        <i class="fas fa-expand"></i>
                                    </button>
                                    <img src="<?= wp_get_attachment_url($block['picture']); ?>" class="avatar">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="column large-10 margin-auto">
                            <div class="counter"> <?= $index + 1 ?> / <?= sizeof($instance['repetidor_fotos']) ?> </div>
                            <div class="legend"><?= $block['legend'] ?></div>
                            <div class="info">
                                <div class="location">
                                    <?= $block['location'] ?>
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="author">
                                    <?= $block['author'] ?>
                                    <i class="fas fa-camera-retro"></i>
                                </div>
                                <div class="author-text">
                                    <?= $block['author_text'] ?>
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>

                            <div class="huge-text">
                                <?= $block['huge_text']?>
                            </div>
                        </div>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </div>

</section>