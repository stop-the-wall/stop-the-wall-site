<?php
/*
  Widget Name: Featured Header
  Description: Header for featured pages
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class FeaturedHeader extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
                
            'icon' => array(
                'type' => 'media',
                'label' => __('Image', 'stop' ),
                'library' => 'image',
            ),

            'hat' => array(
                'type' => 'text',
                'label' => __('Hat', 'stop'),
                // 'description' => 'Deixe vazio para utilizar o título da página'
            ),

            'title' => array(
                'type' => 'text',
                'label' => __('Title', 'stop'),
                // 'description' => 'Deixe vazio para utilizar o título da página'
            ),

        ];

        parent::__construct('featured-header', 'Featured Header', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Header for featured pages'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('featured-header', __FILE__, 'widgets\FeaturedHeader');
