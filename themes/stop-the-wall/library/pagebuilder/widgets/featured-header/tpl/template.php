<div class="featured-header" style="background-image: url('<?= wp_get_attachment_url($instance['icon'], 'full') ?>')">
    <div class="row">
        <div class="column large-4 featured-header--title">
            <div class="featured-header--title-wrapper">
                <div class="hat">
                    <?= $instance['hat'] ?>
                </div>
                <h3>
                    <?= $instance['title'] ?>
                </h3>
            </div>
        </div>
    </div>
</div>