import Vue from 'vue';

const app = new Vue({
    el: '#app',
})

jQuery(document).ready(function(){
    scrolledMenu();

    if(jQuery('.card-special').length > 0){
        jQuery('.main-header').removeClass('scrolled')
    }

    jQuery('.pictures-slider .pictures .itens').slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: true,
        //prevArrow: '<button type="button" data-role="none" class="single-item-collection--attachments-prev" aria-label="Previous" role="button" style="display: block;"></button>',
        //nextArrow: '<button type="button" data-role="none" class="single-item-collection--attachments-next" aria-label="Next" role="button" style="display: block;"></button>',
        prevArrow: jQuery('.single-item-collection--attachments-prev'),
		nextArrow: jQuery('.single-item-collection--attachments-next'),
        //prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>',
        //nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button>'
    
      });


    jQuery('.image-cut img, button.expand-icon').click(function() {
        jQuery(this).closest('.pictures').addClass('active');
        jQuery(this).closest('.pictures').css('height', jQuery(window).height() );
        jQuery('.slider').css('height', jQuery(window).height() );
        
    });

    jQuery('.close-button').click(function() {
        jQuery(this).closest('.pictures').removeClass('active');
        jQuery('.slider').css('height', 'auto' );
        jQuery('.pictures').css('height', 'auto' );
    });

    // jQuery(window).on('resize', function() {
    //     jQuery('.pictures').css('height', jQuery(window).height() );
    //     jQuery('.slider').css('height', jQuery(window).height() );
    // });
})

jQuery(window).scroll(function(){
    scrolledMenu();
})

function scrolledMenu(){
    if(jQuery(window).scrollTop() > 0){
        jQuery('.main-header').addClass('scrolled')
    }else if(jQuery('.card-special').length > 0){
        jQuery('.main-header').removeClass('scrolled')
    }
}