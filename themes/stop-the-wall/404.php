<?php get_header(); ?>
<div class="error-404">
        <h1><?php _e('erro', 'stop') ?><br><strong>404</strong></h1>
        <p><?php _e('Página não encontrada', 'stop') ?></p>
        <a href="/" class="button"> <span><?php _e('Voltar para a página inicial', 'stop') ?></span> </a>
    </div>

<?php get_footer(); ?>
