<!DOCTYPE html>
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1.0">
    <?php wp_head() ?>
    <title><?= is_front_page() ? get_bloginfo('name') : wp_title() ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon.ico?v=2" />
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
</head>

<body <?php body_class(); ?>>
    <header class="header scrolled">
        <div class="header--wrapper">
            <div class="sub-header">
                <div class="row">
                    <div class="column large-12">
                        <div class="sub-header--wrapper">
                            <div class="social-menu">
                                <?php stop\the_social_networks_menu(false) ?>
                            </div>
                            <div class="language-menu">
                                <?php if(function_exists('pll_the_languages')): ?>
                                    <ul> 
                                        <?php pll_the_languages(['display_names_as' => 'slug']) ?> 
                                    </ul>
                                <?php else: ?>
                                    Install polylang plugin to enable the language bar'
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-header">
                <div class="row">
                    <div class="column large-12 content">
                        <div class="display-flex display-none" style="visibility: hidden;">
                            <div class="menu">
                                <button class="menu-btn"></button>
                                <span class="menu-btn-txt">MENU</span>
                                <div class="menus-wrapper">
                                    <?= wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'menu_id' => 'main-menu', 'menu_class' => '_menu']) ?>
                                </div>
                            </div>
                            <div class="search">
                                <i class="fas fa-search"></i>
                                <span>
                                    Search
                                </span>
                            </div>
                        </div>
                        <div class="logo">
                            <a href="/">
                                <div class="logo--text">
                                    <span><?php pll_e('Stop the Wall') ?></span>
                                    <span><?php pll_e('Palestinian Grassroots <strong>Anti-Apartheid Wall</strong> Campaign') ?></span>
                                </div>
                                <div class="logo--image">
                                    <?php
                                    $custom_logo_id = get_theme_mod('custom_logo');
                                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                                    if (has_custom_logo()) {
                                        echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '" width="200">';
                                    } else {
                                    ?>
                                        <img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" width="200" alt="<?= get_bloginfo('name') ?>">
                                    <?php
                                    }
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <!-- <?= wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'menu_id' => 'main-menu', 'menu_class' => 'menu']) ?> -->
        </div>
    </header>
    <div id="app">