</div>
<?php wp_reset_postdata() ?>

<div class="row">
    <div class="column large-12">
        <footer class="main-footer">
            <div class="logo">
                <a href="/">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" alt="Logotipo" height="120">
                    <span>
                        Palestinian Grassroots <br>
                        <strong>
                            Anti-Apartheid Wall 
                        </strong> <br>
                        Campaign
                    </span>
                </a>
            </div>

            <div class="hacklab-site mt-40">
                <?php guaraci\template_part('site-by-hacklab') ?>
            </div>
        </footer>
    </div>
</div>
<?php wp_footer() ?>

</body>

</html>