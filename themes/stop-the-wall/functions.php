<?php
namespace stop;

require __DIR__ . '/library/settings.php';
require __DIR__ . '/library/images.php';
require __DIR__ . '/library/menus.php';
require __DIR__ . '/library/assets.php';
require __DIR__ . '/library/pagebuilder/index.php';
// descomentar se for um multisite e for utilizar o plugin de sincronização de posts
// require __DIR__ . '/library/mssync.php';

add_theme_support( 'align-wide' );
add_theme_support( 'custom-logo' );
add_theme_support( 'post-thumbnails' );

// descomentar para importar conteúdo
// add_filter( 'force_filtered_html_on_import' , '__return_false' );

// removemos a barra de admin para não cachear no cache de borda 
add_filter( 'show_admin_bar', '__return_false' );

// usamos uma taxonomia autor criada com o plugin Pods
add_action( 'init',  function () {
	remove_post_type_support( 'post', 'author' );
});

// filtro para adicionar mais classes de título
add_filter('widgets\\SectionTitle:css_classes', function($css_classes){
	$css_classes['title-stop'] = __('Título stop', 'stop');
	return $css_classes;
});

function get_archive_title () {
	$s = isset($_GET['s']) ? trim($_GET['s']) : '';

	if($s){
		if (is_tag()) {
			$title = sprintf(__('Busca por "%s" na tag "%s"', 'stop'), $s, single_tag_title('',false));
		} elseif (is_category()) {
			$title = sprintf(__('Busca por "%s" na categoria "%s"', 'stop'), $s, single_cat_title('',false));
		} elseif (is_tax()) {
			$title = sprintf(__('Busca por "%s" em "%s"', 'stop'), $s, single_term_title('',false));
		} else {
			if(is_archive()){
				$title = sprintf(__('Busca por "%s" em "%s"', 'stop'), $s, post_type_archive_title());
			} else {
				$title = sprintf(__('Resultado da busca por "%s"', 'stop'), $s);
			}
		}
	} else {
		if (is_tag()) {
			$title = single_tag_title('Tag: ',false);
		} elseif (is_category()) {
			$title = single_cat_title('Categoria: ',false);
		} elseif (is_tax()) {
			$title = single_term_title('',false);
		} else {
			$title = post_type_archive_title();
		}
	}

	return $title;
}

function register_translatable_strings() {
    if (function_exists('pll_register_string')) {
        pll_register_string('Header Logo Text', 'Palestinian Grassroots <strong>Anti-Apartheid Wall</strong> Campaign', 'Stop The Wall');
    }
}

register_translatable_strings();